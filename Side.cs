﻿using System;

public class Side: Product
{	
	public Side() : base() { }
    public Side(string n, double p) : base(n, p) { }

    private override double GetPrice()
    {
        return tradeIn;
    }

    public override string ToString()
    {
        return base.ToString();
    }
}
