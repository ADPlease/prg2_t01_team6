﻿using System;
public class Beverage : Product
{
    private double tradeIn;

    public double TradeIn
    {
        get { return tradeIn; }
        set { tradeIn = value; }
    }
    public Beverage() : base() { }
    public Beverage(string n, double p, double t) : base(n, p)
    {
        tradeIn = t;
    }

    public override string GetPrice()
    {
        return tradeIn;
    }

    public override string ToString()
    {
        return base.ToString();
    }
}

