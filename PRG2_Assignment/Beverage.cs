﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_Assignment
{
    public class Beverage : Product
    {
        private string name;
        private double price;

        public Beverage() { }

        public Beverage(string n, double p)
        {
            name = n;
            price = p;
        }
        public override double GetPrice()
        {
            return price;
        }
        public override string ToString()
        {
            return "Name:" + name + "Price:" + price;
        }
    }
}

