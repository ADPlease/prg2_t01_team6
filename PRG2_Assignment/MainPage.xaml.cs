﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PRG2_Assignment
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// private List<MenuItem> bundles = new List</MenuItem>();
        List<MenuItem> value = new List<MenuItem>();
        List<MenuItem> sides = new List<MenuItem>();
        List<MenuItem> beverages = new List<MenuItem>();
        List<MenuItem> valuemeal = new List<MenuItem>();
        List<MenuItem> currentItemList = new List<MenuItem>();
        List<MenuItem> bundlemeals = new List<MenuItem>();
        Order order = new Order(1);
        string aaa;
        string bbb;
        public MainPage()
        {
            this.InitializeComponent();
            InitProducts();
            aaa = "";
            aaa = "Welcome to Tom's Cafe \n\n Choose your item from the menu";

            displayText.Text = aaa;
        }

        public void InitProducts()
        {
            // sides

            MenuItem side1 = new MenuItem("Hashbrown", 2.10);
            side1.ProductList.Add(new Side("Hashbrown", 2.10));
            sides.Add(side1);

            MenuItem side2 = new MenuItem("Truffle fries", 3.7);
            side2.ProductList.Add(new Side("Truffle fries", 3.7));
            sides.Add(side2);

            MenuItem side3 = new MenuItem("Calamari", 3.4);
            side3.ProductList.Add(new Side("Calamari", 3.4));
            sides.Add(side3);

            MenuItem side4 = new MenuItem("Caesar Salad", 4.3);
            side4.ProductList.Add(new Side("Caesar Salad", 4.3));
            sides.Add(side4);

            // beverage (adi)

            MenuItem bev1 = new MenuItem("Cola", 2.85);
            bev1.ProductList.Add(new Beverage("Cola", 2.85));
            beverages.Add(bev1);

            MenuItem bev2 = new MenuItem("Green Tea", 3.7);
            bev2.ProductList.Add(new Beverage("Green Tea", 3.7));
            beverages.Add(bev2);

            MenuItem bev3 = new MenuItem("Coffee", 2.7);
            bev3.ProductList.Add(new Beverage("Coffee", 2.7));
            beverages.Add(bev3);

            MenuItem bev4 = new MenuItem("Tea", 2.7);
            bev4.ProductList.Add(new Beverage("Tea", 2.7));
            beverages.Add(bev4);

            MenuItem bev5 = new MenuItem("Tom's Root Beer", 9.70);
            bev5.ProductList.Add(new Beverage("Tom's Root Beer", 9.70));
            beverages.Add(bev5);

            MenuItem bev6 = new MenuItem("Mocktail", 15.90);
            bev6.ProductList.Add(new Beverage("Mocktail", 15.90));
            beverages.Add(bev6);

            // value meal (adi)

            MenuItem vm1 = new MenuItem("Hotcake with Sausage", 6.90);
            ValueMeal vmm = new ValueMeal("Hotcake with Sausage", 6.90, Convert.ToDateTime("7:00:00AM"), Convert.ToDateTime("02:00:00PM"));
            if (vmm.IsAvailable())
            {
                vm1.ProductList.Add(vmm);
                valuemeal.Add(vm1);
            }

            MenuItem vm2 = new MenuItem("Hamburger", 7.50);
            vmm = new ValueMeal("Hamburger", 7.50, Convert.ToDateTime("10:00:00AM"), Convert.ToDateTime("07:00:00PM"));
            if (vmm.IsAvailable())
            {
                vm2.ProductList.Add(vmm);
                valuemeal.Add(vm2);
            }

            MenuItem vm3 = new MenuItem("Nasi Lemak", 5.40);
            vmm = new ValueMeal("Nasi Lemak", 5.40, Convert.ToDateTime("12:00:00AM"), Convert.ToDateTime("12:00:00AM"));
            if (vmm.IsAvailable())
            {
                vm3.ProductList.Add(vmm);
                valuemeal.Add(vm3);
            }

            MenuItem vm4 = new MenuItem("Ribeye Steak", 10.20);
            vmm = new ValueMeal("Ribeye Steak", 10.20, Convert.ToDateTime("04:00:00PM"), Convert.ToDateTime("10:00:00PM"));
            if (vmm.IsAvailable())
            {
                vm4.ProductList.Add(vmm);
                valuemeal.Add(vm4);
            }
            // bundles meal (adi)

            MenuItem bundle1 = new MenuItem("Breakfast Set", 7.90);
            if (vm1.ProductList.Count > 0)
            {
                bundle1.ProductList.Add(vm1.ProductList[0]);
                bundle1.ProductList.Add(side1.ProductList[0]);
                bundlemeals.Add(bundle1);
            }

            //
            MenuItem bundle2 = new MenuItem("Hamburger Combo", 10.20);
            if (vm2.ProductList.Count > 0)
            {

                bundle2.ProductList.Add(vm2.ProductList[0]);
                bundle2.ProductList.Add(side2.ProductList[0]);
                bundle2.ProductList.Add(bev1.ProductList[0]);
                bundlemeals.Add(bundle2);
            }

            MenuItem bundle3 = new MenuItem("Dinner Set", 18.50);
            if (vm4.ProductList.Count > 0)
            {
                bundle3.ProductList.Add(vm4.ProductList[0]);
                bundle3.ProductList.Add(side2.ProductList[0]);
                bundle3.ProductList.Add(side4.ProductList[0]);
                bundle3.ProductList.Add(bev3.ProductList[0]);
                bundlemeals.Add(bundle3);
            }
            bundlemeals.Add(bundle3);

        }
        private void mainsButton_Click(object sender, RoutedEventArgs e)
        {
            itemsListView.ItemsSource = valuemeal;
            currentItemList = valuemeal;

        }

        private void bundlesButton_Click(object sender, RoutedEventArgs e)
        {

            itemsListView.ItemsSource = bundlemeals;
            currentItemList = bundlemeals;
        }

        private void sidesButton_Click(object sender, RoutedEventArgs e)
        {
            itemsListView.ItemsSource = sides;
            currentItemList = sides;

        }

        private void beveragesButton_Click(object sender, RoutedEventArgs e)
        {
            itemsListView.ItemsSource = beverages;
            currentItemList = beverages;
        }

        private void addButton_Click(object sender, RoutedEventArgs e) /// aloysius did the part where it would show up after clicking add (adi did the try and catch to prevent crashing if buttons when theres nothing
        {
            try
            {
                MenuItem menuItem = currentItemList[itemsListView.SelectedIndex];//Current selection
                OrderItem orderItem = new OrderItem(1, menuItem);//new item

                foreach (OrderItem oi in order.ItemList)
                {
                    if (oi.Item.Equals(menuItem))
                    {
                        oi.AddQty();
                        cartsListView.ItemsSource = null;//refreshes cart
                        cartsListView.ItemsSource = order.ItemList;

                        displayText.Text = menuItem.Name + " Added " + "\n" + " Total " + "\n" + order.GetTotalAmount();
                        return;
                    }
                }

                order.Add(orderItem);
                cartsListView.ItemsSource = null;//refreshes cart
                cartsListView.ItemsSource = order.ItemList;
                displayText.Text = menuItem.Name + " Added " + "\n" + " Total " + "\n" + order.GetTotalAmount();
            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }

        private void itemsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void orderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // receipt 
                string receipt = "";

                receipt += "Receipt #" + order.OrderNo + "\n";
                receipt += DateTime.Now + "\n\n";

                foreach (OrderItem oi in order.ItemList)
                {
                    receipt += oi.Quantity + " " + oi.Item.Name + " $" + oi.GetItemTotalAmt() + "\n";
                }

                receipt += "Total: $" + order.GetTotalAmount();

                displayText.Text = receipt;

                // increase order number
                order = new Order(order.OrderNo + 1);

                order.ItemList.Clear();
                cartsListView.ItemsSource = null;//refreshes cart
                cartsListView.ItemsSource = order.ItemList;
            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (order.ItemList.Count == 0)
                {
                    displayText.Text = " There is nothing in your cart. ";
                }
                else
                {
                    order.ItemList.Clear();
                    cartsListView.ItemsSource = null;//refreshes cart
                    cartsListView.ItemsSource = order.ItemList;
                    displayText.Text = " Your order has been cancelled ";
                }
            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }

        private void removeButton_Click(object sender, RoutedEventArgs e) ///aloysius and adi
        {
            try
            {
                if (order.ItemList.Count == 0)
                {
                    displayText.Text = " There is nothing in your cart. ";
                }
                OrderItem ori = order.ItemList[cartsListView.SelectedIndex];

                if (!ori.RemoveQty())
                {
                    order.Remove(cartsListView.SelectedIndex);
                }
                cartsListView.ItemsSource = null;//refreshes cart
                cartsListView.ItemsSource = order.ItemList;


                displayText.Text = ori.Item + " Removed ";
            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }







        private void cartsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void displayText_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }
    }

}
