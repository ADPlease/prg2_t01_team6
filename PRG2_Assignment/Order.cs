﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_Assignment
{
    class Order
    {
        private int orderNo;
        private List<OrderItem> itemList;

        public int OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }

        public List<OrderItem> ItemList
        {
            get { return itemList; }
            set { itemList = value; }
        }
        public Order() { }
        public Order(int o)
        {
            orderNo = o;
            itemList = new List<OrderItem>();
        }

        public void Add(OrderItem item)
        {
            itemList.Add(item);
        }

        public void Remove(int index)
        {
            itemList.RemoveAt(index);
        }

        public double GetTotalAmount()
        {
            double total = 0;
            foreach (OrderItem oi in itemList)
            {
                total += oi.GetItemTotalAmt();
            }
            return total;
        }

        public override string ToString()
        {
            return "Order:" + orderNo + "ItemList" + itemList;
        }

    }
}
