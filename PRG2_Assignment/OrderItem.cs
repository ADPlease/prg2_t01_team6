﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_Assignment
{
    class OrderItem
    {
        private int quantity;
        private MenuItem item;

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        public MenuItem Item
        {
            get { return item; }
            set { item = value; }
        }

        public OrderItem() { }
        public OrderItem(int q, MenuItem i)
        {
            Quantity = q;
            Item = i;
        }
        public void AddQty()
        {
            Quantity++;
        }

        public bool RemoveQty()
        {
            if (Quantity < 2)
            {
                return false;
            }

            Quantity--;
            return true;
        }
        public double GetItemTotalAmt()
        {
          return Item.Price * Quantity;
        }
        public override string ToString()
        {
            return item + " X " + quantity;
        }

    }
}
