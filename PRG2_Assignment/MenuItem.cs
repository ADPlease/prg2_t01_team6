﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_Assignment
{
    class MenuItem
    {
        private string name;
        private double price;

        private List<Product> productList;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public List<Product> ProductList
        {
            get { return productList; }
            set { productList = value; }
        }

        public MenuItem() { }
        public MenuItem(string n, double p)
        {
            name = n;
            price = p;
            productList = new List<Product>();
        }
// gg
        public double GetTotalPrice()
        {
            double total = 0;
            foreach (Product product in productList) {
              total += product.GetPrice();
            }
            return total;
        }
        public override string ToString()
        {
            return name +  " \n "  + price;
        }

    }
}
