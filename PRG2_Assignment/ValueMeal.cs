﻿using System;

namespace PRG2_Assignment
{
    public class ValueMeal : Product
    {
        private DateTime startTime;
        private DateTime endTime;


        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }


        public DateTime EndTime
        {
            get { return endTime; }
            set
            {
                endTime = value;
            }
        }


        public ValueMeal() : base() { }
        public ValueMeal(string n, double p, DateTime s, DateTime e) : base(n, p)
        {
            startTime = s;
            endTime = e;
        }

        public override double GetPrice()
        {
            return Price;
        }

        public bool IsAvailable()
        {
            DateTime now = DateTime.Now;
            if (startTime.TimeOfDay <= now.TimeOfDay && now.TimeOfDay <= endTime.TimeOfDay)
                return true;
            else
                return false;

        }
        public override string ToString()
        {
            return ToString();
        }
    }
}  